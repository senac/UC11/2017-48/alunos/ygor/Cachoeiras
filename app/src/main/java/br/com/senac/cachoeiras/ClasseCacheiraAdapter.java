package br.com.senac.cachoeiras;


import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ClasseCacheiraAdapter  extends BaseAdapter{
Context context;
    List<ClasseCachoeira> cachoeiras;

    public ClasseCacheiraAdapter(Context context, List<ClasseCachoeira> cachoeiras) {
        this.context = context;
        this.cachoeiras = cachoeiras;
    }

    @Override
    public int getCount() {
        return cachoeiras.size();
    }

    @Override
    public Object getItem(int i) {
        return cachoeiras.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
      ClasseCachoeira c = cachoeiras.get(i);


  TextView txtnome;
        view  = LayoutInflater.from(context).inflate(R.layout.item_lista,viewGroup,false);
        TextView nome = view.findViewById(R.id.nome);
        nome.setText(c.getNome());



        return view;
    }
}
