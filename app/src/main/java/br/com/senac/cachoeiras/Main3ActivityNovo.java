package br.com.senac.cachoeiras;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

public class Main3ActivityNovo extends AppCompatActivity {

    public  static  final String CACHOEIRA = "cachoeira";

ClasseCachoeira cachoeira;
final ArrayList<ClasseCachoeira> mylist = (ArrayList<ClasseCachoeira>)getIntent().getSerializableExtra("listacachoeiras");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3_novo);

        final Button salvar =  (Button) findViewById(R.id.salvar);
        final EditText nome = (EditText) findViewById(R.id.nome);
        final EditText informacoes = (EditText) findViewById(R.id.informacoes);
       final RatingBar stars = findViewById(R.id.stars);






        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = mylist.size();
                String nomecachoeira = nome.getText().toString();
                String info = informacoes.getText().toString();
                double ratingstars =  stars.getRating() ;



                cachoeira = new ClasseCachoeira(id,nomecachoeira,info,ratingstars);
                Intent intent = new Intent();
                intent.putExtra(CACHOEIRA, cachoeira);
                setResult(RESULT_OK,intent);

                finish();


            }

        });


    }






}
