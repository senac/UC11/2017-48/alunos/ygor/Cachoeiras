package br.com.senac.cachoeiras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
   private ArrayAdapter adapter;



    public static final String  CACHOEIRA = "Cachoeira";
    public static final int NOVO = 1;

    private List<ClasseCachoeira> cachoeiras= new ArrayList<ClasseCachoeira>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);



        final ListView listacachoeiras = findViewById(R.id.listadecachoeiras);






        adapter = new ArrayAdapter<ClasseCachoeira>(this,android.R.layout.simple_list_item_1,cachoeiras);
        listacachoeiras.setAdapter(adapter);

        listacachoeiras.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(view.getContext(),cachoeiras.get(i).getNome(),Toast.LENGTH_SHORT).show();
                 Intent intentcachoeiras = new Intent();
                 intentcachoeiras.putExtra(CACHOEIRA, (Serializable) cachoeiras);
                 startActivity(intentcachoeiras);
            }
        });


    }
        public boolean onCreateOptionsMenu(Menu menu){
            MenuInflater inflater = getMenuInflater();

        inflater.inflate (R.menu.cachoeirasmenu,menu);

            return true;
    }
    public void novo(MenuItem item){
        Intent intent = new Intent(MainActivity.this, Main3ActivityNovo.class);

        startActivity(intent);


    }
    public void sobre(MenuItem item){
        Intent intent = new Intent(MainActivity.this, null);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()){
          case R.id.novo:
              Log.i("#MENU","Clicou novo");
              break;
          case R.id.sobre:
              Log.i("#MENU","Clicou Sobre");
              break;

      }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            ClasseCachoeira cachoeira = (ClasseCachoeira) data.getSerializableExtra(Main3ActivityNovo.CACHOEIRA);
            cachoeiras.add(cachoeira);
            adapter.notifyDataSetChanged();


            Toast.makeText(this, "Salvo", Toast.LENGTH_LONG).show();
        }
    }


}



