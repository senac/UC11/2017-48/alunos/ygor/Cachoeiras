package br.com.senac.cachoeiras;


import java.io.Serializable;

public class ClasseCachoeira implements Serializable {

    private String nome;
    private String Informacoes;
    private Double classificacao;
    private  int imagem;
    private int id ;

    public ClasseCachoeira(int id,String nome, String informacoes, Double classificacao) {
        this.nome = nome;
        Informacoes = informacoes;
        this.classificacao = classificacao;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInformacoes() {
        return Informacoes;
    }

    public void setInformacoes(String informacoes) {
        Informacoes = informacoes;
    }

    public Double getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(Double classificacao) {
        this.classificacao = classificacao;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }
}
